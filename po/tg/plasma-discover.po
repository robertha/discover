# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the discover package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-09 00:47+0000\n"
"PO-Revision-Date: 2020-08-27 11:51+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: English <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: discover/DiscoverObject.cpp:159
#, kde-format
msgid ""
"Discover currently cannot be used to install any apps or perform system "
"updates because none of its app backends are available."
msgstr ""

#: discover/DiscoverObject.cpp:163
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can install some on the Settings page, under the <interface>Missing "
"Backends</interface> section.<nl/><nl/>Also please consider reporting this "
"as a packaging issue to your distribution."
msgstr ""

#: discover/DiscoverObject.cpp:168 discover/DiscoverObject.cpp:380
#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgid "Report This Issue"
msgstr ""

#: discover/DiscoverObject.cpp:173
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can use <command>pacman</command> to install the optional dependencies "
"that are needed to enable the application backends.<nl/><nl/>Please note "
"that Arch Linux developers recommend using <command>pacman</command> for "
"managing software because the PackageKit backend is not well-integrated on "
"Arch Linux."
msgstr ""

#: discover/DiscoverObject.cpp:181
#, kde-format
msgid "Learn More"
msgstr ""

#: discover/DiscoverObject.cpp:269
#, kde-format
msgid "Could not find category '%1'"
msgstr ""

#: discover/DiscoverObject.cpp:284
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr ""

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""

#: discover/DiscoverObject.cpp:310
#, fuzzy, kde-format
#| msgid "Couldn't open %1"
msgid "Could not open %1"
msgstr "Кушодани %1 қатъ шуд"

#: discover/DiscoverObject.cpp:372
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr ""

#: discover/DiscoverObject.cpp:374
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""

#: discover/DiscoverObject.cpp:377
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr ""

#: discover/DiscoverObject.cpp:442 discover/DiscoverObject.cpp:443
#: discover/main.cpp:121 discover/qml/BrowsingPage.qml:20
#, kde-format
msgid "Discover"
msgstr "Кашфиёт"

#: discover/DiscoverObject.cpp:443
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""

#: discover/main.cpp:43
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr ""

#: discover/main.cpp:44
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr ""

#: discover/main.cpp:45
#, kde-format
msgid "Display a list of entries with a category."
msgstr ""

#: discover/main.cpp:46
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr ""

#: discover/main.cpp:47
#, kde-format
msgid "List all the available modes."
msgstr ""

#: discover/main.cpp:48
#, kde-format
msgid "Compact Mode (auto/compact/full)."
msgstr ""

#: discover/main.cpp:49
#, kde-format
msgid "Local package file to install"
msgstr ""

#: discover/main.cpp:50
#, kde-format
msgid "List all the available backends."
msgstr ""

#: discover/main.cpp:51
#, kde-format
msgid "Search string."
msgstr ""

#: discover/main.cpp:52
#, kde-format
msgid "Lists the available options for user feedback"
msgstr ""

#: discover/main.cpp:54
#, kde-format
msgid "Supports appstream: url scheme"
msgstr ""

#: discover/main.cpp:123
#, kde-format
msgid "An application explorer"
msgstr "Намоишгари барномаҳо"

#: discover/main.cpp:125
#, fuzzy, kde-format
#| msgid "© 2010-2019 Plasma Development Team"
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2019 Дастаи барномасозони Плазма"

#: discover/main.cpp:126
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr ""

#: discover/main.cpp:127
#, kde-format
msgid "Nate Graham"
msgstr ""

#: discover/main.cpp:128
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr ""

#: discover/main.cpp:132
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr ""

#: discover/main.cpp:133
#, kde-format
msgid "KNewStuff"
msgstr ""

#: discover/main.cpp:140
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Victor Ibragimov"

#: discover/main.cpp:140
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "victor.ibragimov@gmail.com"

#: discover/main.cpp:153
#, kde-format
msgid "Available backends:\n"
msgstr "Коркардкунандаи дастрас:\n"

#: discover/main.cpp:209
#, kde-format
msgid "Available modes:\n"
msgstr "Реҷаҳои дастрас:\n"

#: discover/qml/AddonsView.qml:18 discover/qml/navigation.js:43
#, fuzzy, kde-format
#| msgid "Addons"
msgid "Addons for %1"
msgstr "Барномаи иловагӣ"

#: discover/qml/AddonsView.qml:50
#, kde-format
msgid "More…"
msgstr ""

#: discover/qml/AddonsView.qml:59
#, kde-format
msgid "Apply Changes"
msgstr "Тағйиротро татбиқ кардан"

#: discover/qml/AddonsView.qml:66
#, kde-format
msgid "Reset"
msgstr ""

#: discover/qml/AddSourceDialog.qml:20
#, fuzzy, kde-format
#| msgid "Add a new %1 repository"
msgid "Add New %1 Repository"
msgstr "Илова кардани анбори додаҳои нави %1"

#: discover/qml/AddSourceDialog.qml:44
#, kde-format
msgid "Add"
msgstr "Илова кардан"

#: discover/qml/AddSourceDialog.qml:49 discover/qml/DiscoverWindow.qml:257
#: discover/qml/InstallApplicationButton.qml:41
#: discover/qml/ProgressView.qml:104 discover/qml/SourcesPage.qml:179
#: discover/qml/UpdatesPage.qml:253 discover/qml/WebflowDialog.qml:39
#, kde-format
msgid "Cancel"
msgstr "Бекор кардан"

#: discover/qml/ApplicationDelegate.qml:140
#: discover/qml/ApplicationPage.qml:192
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 баҳо"
msgstr[1] "%1 баҳо"

#: discover/qml/ApplicationDelegate.qml:140
#: discover/qml/ApplicationPage.qml:192
#, kde-format
msgid "No ratings yet"
msgstr "То ҳол ягон баҳо нест"

#: discover/qml/ApplicationPage.qml:59
#, kde-format
msgid "Sources"
msgstr "Манбаъҳо"

#: discover/qml/ApplicationPage.qml:70
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:174
#, kde-format
msgid "Unknown author"
msgstr ""

#: discover/qml/ApplicationPage.qml:204
#, kde-format
msgid "Could not access the screenshots"
msgstr ""

#: discover/qml/ApplicationPage.qml:261
#, fuzzy, kde-format
#| msgid "Version:"
msgid "Version"
msgstr "Нашр:"

#: discover/qml/ApplicationPage.qml:288
#: discover/qml/ApplicationsListPage.qml:109
#, kde-format
msgid "Size"
msgstr "Андоза"

#: discover/qml/ApplicationPage.qml:315
#, kde-format
msgid "Distributed by"
msgstr ""

#: discover/qml/ApplicationPage.qml:342
#, fuzzy, kde-format
#| msgid "License:"
msgid "License"
msgid_plural "Licenses"
msgstr[0] "Иҷозатнома:"
msgstr[1] "Иҷозатнома:"

#: discover/qml/ApplicationPage.qml:351
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr ""

#: discover/qml/ApplicationPage.qml:389
#, fuzzy, kde-format
#| msgid "What's New"
msgid "What does this mean?"
msgstr "Навигариҳо"

#: discover/qml/ApplicationPage.qml:399
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] ""
msgstr[1] ""

#: discover/qml/ApplicationPage.qml:417 discover/qml/ApplicationPage.qml:858
#, fuzzy, kde-format
#| msgid "Enter a rating"
msgid "Content Rating"
msgstr "Баҳо диҳед"

#: discover/qml/ApplicationPage.qml:427
#, kde-format
msgid "Age: %1+"
msgstr ""

#: discover/qml/ApplicationPage.qml:451
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr ""

#: discover/qml/ApplicationPage.qml:547
#, fuzzy, kde-format
#| msgid "Documentation:"
msgid "Documentation"
msgstr "Ҳуҷҷатнигорӣ:"

#: discover/qml/ApplicationPage.qml:548
#, kde-format
msgid "Read the project's official documentation"
msgstr ""

#: discover/qml/ApplicationPage.qml:564
#, kde-format
msgid "Website"
msgstr ""

#: discover/qml/ApplicationPage.qml:565
#, kde-format
msgid "Visit the project's website"
msgstr ""

#: discover/qml/ApplicationPage.qml:581
#, kde-format
msgid "Addons"
msgstr "Барномаи иловагӣ"

#: discover/qml/ApplicationPage.qml:582
#, kde-format
msgid "Install or remove additional functionality"
msgstr ""

#: discover/qml/ApplicationPage.qml:601
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr ""

#: discover/qml/ApplicationPage.qml:602
#, kde-format
msgid "Send a link for this application"
msgstr ""

#: discover/qml/ApplicationPage.qml:618
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr ""

#: discover/qml/ApplicationPage.qml:638
#, kde-format
msgid "What's New"
msgstr "Навигариҳо"

#: discover/qml/ApplicationPage.qml:668
#, fuzzy, kde-format
#| msgid "Reviews"
msgid "Loading reviews for %1"
msgstr "Тақризҳо"

#: discover/qml/ApplicationPage.qml:674
#, kde-format
msgid "Reviews"
msgstr "Тақризҳо"

#: discover/qml/ApplicationPage.qml:712
#, fuzzy, kde-format
#| msgid "Review..."
msgid "Show all %1 Reviews"
msgid_plural "Show all %1 Reviews"
msgstr[0] "Тақриз..."
msgstr[1] "Тақриз..."

#: discover/qml/ApplicationPage.qml:724
#, fuzzy, kde-format
#| msgid "Write a review!"
msgid "Write a Review"
msgstr "Тақризеро нависед!"

#: discover/qml/ApplicationPage.qml:724
#, fuzzy, kde-format
#| msgid "Write a review!"
msgid "Install to Write a Review"
msgstr "Тақризеро нависед!"

#: discover/qml/ApplicationPage.qml:736
#, fuzzy, kde-format
#| msgid "Get involved:"
msgid "Get Involved"
msgstr "Ҷалб шавед:"

#: discover/qml/ApplicationPage.qml:769
#, fuzzy, kde-format
#| msgid "Donate:"
msgid "Donate"
msgstr "Кумакпулӣ:"

#: discover/qml/ApplicationPage.qml:770
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr ""

#: discover/qml/ApplicationPage.qml:786
#, kde-format
msgid "Report Bug"
msgstr ""

#: discover/qml/ApplicationPage.qml:787
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr ""

#: discover/qml/ApplicationPage.qml:801
#, fuzzy, kde-format
#| msgid "More..."
msgid "Contribute"
msgstr "Бештар..."

#: discover/qml/ApplicationPage.qml:802
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr ""

#: discover/qml/ApplicationPage.qml:827
#, fuzzy, kde-format
#| msgid "License:"
msgid "All Licenses"
msgstr "Иҷозатнома:"

#: discover/qml/ApplicationPage.qml:872
#, kde-format
msgid "Risks of proprietary software"
msgstr ""

#: discover/qml/ApplicationPage.qml:878
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""

#: discover/qml/ApplicationPage.qml:879
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:53
#, fuzzy, kde-format
#| msgid "Search: %1"
msgid "Search: %1 - %2 items"
msgstr "Ҷустуҷӯ: %1"

#: discover/qml/ApplicationsListPage.qml:55
#, kde-format
msgid "Search: %1"
msgstr "Ҷустуҷӯ: %1"

#: discover/qml/ApplicationsListPage.qml:59
#, fuzzy, kde-format
#| msgid "%1 - %2"
msgid "%1 - %2 items"
msgstr "%1 - %2"

#: discover/qml/ApplicationsListPage.qml:65
#, fuzzy, kde-format
#| msgid "Search: %1"
msgid "Search - %1 items"
msgstr "Ҷустуҷӯ: %1"

#: discover/qml/ApplicationsListPage.qml:67
#: discover/qml/ApplicationsListPage.qml:207
#, kde-format
msgid "Search"
msgstr "Ҷустуҷӯ"

#: discover/qml/ApplicationsListPage.qml:88
#, kde-format
msgid "Sort: %1"
msgstr "Тартибдиҳӣ аз рӯи: %1"

#: discover/qml/ApplicationsListPage.qml:91
#, kde-format
msgid "Name"
msgstr "Ном"

#: discover/qml/ApplicationsListPage.qml:100
#, kde-format
msgid "Rating"
msgstr "Баҳо"

#: discover/qml/ApplicationsListPage.qml:118
#, kde-format
msgid "Release Date"
msgstr "Санаи нашр"

#: discover/qml/ApplicationsListPage.qml:173
#, fuzzy, kde-format
#| msgid "Sorry, nothing found"
msgid "Nothing found"
msgstr "Мутаассифона, ягон чиз ёфт нашуд"

#: discover/qml/ApplicationsListPage.qml:185
#, kde-format
msgctxt "%1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:186
#, kde-format
msgctxt "%1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:188
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Search the web for \"%1\""
msgstr ""

#: discover/qml/ApplicationsListPage.qml:192
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:222
#, fuzzy, kde-format
#| msgid "Still looking..."
msgid "Still looking…"
msgstr "Дар ҳоли ҷустуҷӯ..."

#: discover/qml/BrowsingPage.qml:49
#, kde-format
msgid "Unable to load applications"
msgstr ""

#: discover/qml/BrowsingPage.qml:88
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr ""

#: discover/qml/BrowsingPage.qml:106
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr ""

#: discover/qml/BrowsingPage.qml:120
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr ""

#: discover/qml/BrowsingPage.qml:139 discover/qml/BrowsingPage.qml:168
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr ""

#: discover/qml/BrowsingPage.qml:149
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr ""

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr ""

#: discover/qml/DiscoverWindow.qml:56
#, fuzzy, kde-format
#| msgid "Homepage:"
msgid "&Home"
msgstr "Сомонаи расмӣ:"

#: discover/qml/DiscoverWindow.qml:66
#, fuzzy, kde-format
#| msgid "Search"
msgid "&Search"
msgstr "Ҷустуҷӯ"

#: discover/qml/DiscoverWindow.qml:74
#, fuzzy, kde-format
#| msgid "Installed"
msgid "&Installed"
msgstr "Насбшуда"

#: discover/qml/DiscoverWindow.qml:81
#, fuzzy, kde-format
#| msgid "Fetching updates..."
msgid "Fetching &updates…"
msgstr "Дар ҳоли бозёбии навсозиҳо..."

#: discover/qml/DiscoverWindow.qml:81
#, fuzzy, kde-format
#| msgid "Up to date"
msgid "&Up to date"
msgstr "Нав аст"

#: discover/qml/DiscoverWindow.qml:81
#, fuzzy, kde-format
#| msgctxt "Update section name"
#| msgid "Update (%1)"
msgctxt "Update section name"
msgid "&Update (%1)"
msgstr "Навсозӣ (%1)"

#: discover/qml/DiscoverWindow.qml:88
#, fuzzy, kde-format
#| msgid "About"
msgid "&About"
msgstr "Дар бораи барнома"

#: discover/qml/DiscoverWindow.qml:96
#, fuzzy, kde-format
#| msgid "Settings"
msgid "S&ettings"
msgstr "Танзимот"

#: discover/qml/DiscoverWindow.qml:146 discover/qml/DiscoverWindow.qml:325
#: discover/qml/DiscoverWindow.qml:432
#, kde-format
msgid "Error"
msgstr ""

#: discover/qml/DiscoverWindow.qml:150
#, kde-format
msgid "Unable to find resource: %1"
msgstr "Манбаи зерин ёфт нашуд: %1"

#: discover/qml/DiscoverWindow.qml:244 discover/qml/SourcesPage.qml:169
#, kde-format
msgid "Proceed"
msgstr "Иҷро кардан"

#: discover/qml/DiscoverWindow.qml:301
#, kde-format
msgid "Report this issue"
msgstr ""

#: discover/qml/DiscoverWindow.qml:325
#, kde-format
msgid "Error %1 of %2"
msgstr ""

#: discover/qml/DiscoverWindow.qml:370
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr ""

#: discover/qml/DiscoverWindow.qml:383
#, fuzzy, kde-format
#| msgid "Show contents"
msgctxt "@action:button"
msgid "Show Next"
msgstr "Намоиш додани муҳтаво"

#: discover/qml/DiscoverWindow.qml:399 discover/qml/UpdatesPage.qml:98
#, kde-format
msgid "Copy to Clipboard"
msgstr ""

#: discover/qml/Feedback.qml:12
#, kde-format
msgid "Submit usage information"
msgstr ""

#: discover/qml/Feedback.qml:13
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""

#: discover/qml/Feedback.qml:17
#, fuzzy, kde-format
#| msgid "Submitting usage information..."
msgid "Submitting usage information…"
msgstr "Дар ҳоли пешниҳоди иттилооти истифодабарӣ..."

#: discover/qml/Feedback.qml:17
#, kde-format
msgid "Configure"
msgstr "Танзимот"

#: discover/qml/Feedback.qml:21
#, fuzzy, kde-format
#| msgid "Configure"
msgid "Configure feedback…"
msgstr "Танзимот"

#: discover/qml/Feedback.qml:28 discover/qml/SourcesPage.qml:19
#, fuzzy, kde-format
#| msgid "Configure"
msgid "Configure Updates…"
msgstr "Танзимот"

#: discover/qml/Feedback.qml:58
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""

#: discover/qml/Feedback.qml:58
#, fuzzy, kde-format
#| msgid "More..."
msgid "Contribute…"
msgstr "Бештар..."

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "We are looking for your feedback!"
msgstr ""

#: discover/qml/Feedback.qml:63
#, fuzzy, kde-format
#| msgid "Participate..."
msgid "Participate…"
msgstr "Иштирок кардан..."

#: discover/qml/InstallApplicationButton.qml:25
#, fuzzy, kde-format
#| msgid "Loading..."
msgctxt "State being fetched"
msgid "Loading…"
msgstr "Дар ҳоли боркунӣ..."

#: discover/qml/InstallApplicationButton.qml:28
#, kde-format
msgid "Install"
msgstr "Насб кардан"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgid "Remove"
msgstr "Тоза кардан"

#: discover/qml/InstalledPage.qml:15
#, kde-format
msgid "Installed"
msgstr "Насбшуда"

#: discover/qml/navigation.js:19
#, kde-format
msgid "Resources for '%1'"
msgstr "Манбаъҳо барои '%1'"

#: discover/qml/ProgressView.qml:17
#, kde-format
msgid "Tasks (%1%)"
msgstr "Вазифаҳо (%1%)"

#: discover/qml/ProgressView.qml:17 discover/qml/ProgressView.qml:40
#, kde-format
msgid "Tasks"
msgstr "Вазифаҳо"

#: discover/qml/ProgressView.qml:97
#, fuzzy, kde-format
#| msgctxt "TransactioName - TransactionStatus"
#| msgid "%1 - %2: %3"
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:98
#, fuzzy, kde-format
#| msgctxt "TransactioName - TransactionStatus"
#| msgid "%1 - %2: %3"
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:99
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:61
#, kde-format
msgid "unknown reviewer"
msgstr "мушоҳидакунандаи номаълум"

#: discover/qml/ReviewDelegate.qml:62
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b> аз ҷониби %2"

#: discover/qml/ReviewDelegate.qml:62
#, kde-format
msgid "Comment by %1"
msgstr "Шарҳ аз ҷониби %1"

#: discover/qml/ReviewDelegate.qml:80
#, kde-format
msgid "Version: %1"
msgstr "Нашр: %1"

#: discover/qml/ReviewDelegate.qml:80
#, fuzzy, kde-format
#| msgid "Version: %1"
msgid "Version: unknown"
msgstr "Нашр: %1"

#: discover/qml/ReviewDelegate.qml:95
#, kde-format
msgid "Votes: %1 out of %2"
msgstr ""

#: discover/qml/ReviewDelegate.qml:102
#, kde-format
msgid "Was this review useful?"
msgstr ""

#: discover/qml/ReviewDelegate.qml:114
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "Ҳа"

#: discover/qml/ReviewDelegate.qml:131
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "Не"

#: discover/qml/ReviewDialog.qml:18
#, fuzzy, kde-format
#| msgid "Reviews"
msgid "Reviewing %1"
msgstr "Тақризҳо"

#: discover/qml/ReviewDialog.qml:24
#, kde-format
msgid "Submit review"
msgstr "Пешниҳод кардани тақриз"

#: discover/qml/ReviewDialog.qml:37
#, kde-format
msgid "Rating:"
msgstr "Баҳо:"

#: discover/qml/ReviewDialog.qml:42
#, kde-format
msgid "Name:"
msgstr "Ном:"

#: discover/qml/ReviewDialog.qml:50
#, kde-format
msgid "Title:"
msgstr "Сарлавҳа:"

#: discover/qml/ReviewDialog.qml:67
#, kde-format
msgid "Enter a rating"
msgstr "Баҳо диҳед"

#: discover/qml/ReviewDialog.qml:68
#, kde-format
msgid "Write the title"
msgstr "Сарлавҳаро нависед"

#: discover/qml/ReviewDialog.qml:69
#, fuzzy, kde-format
#| msgid "Write a review!"
msgid "Write the review"
msgstr "Тақризеро нависед!"

#: discover/qml/ReviewDialog.qml:70
#, fuzzy, kde-format
#| msgid "Keep writing..."
msgid "Keep writing…"
msgstr "Нависед..."

#: discover/qml/ReviewDialog.qml:71
#, kde-format
msgid "Too long!"
msgstr "Хеле дароз!"

#: discover/qml/ReviewDialog.qml:72
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr ""

#: discover/qml/ReviewsPage.qml:42
#, fuzzy, kde-format
#| msgid "Reviews"
msgid "Reviews for %1"
msgstr "Тақризҳо"

#: discover/qml/ReviewsPage.qml:53
#, fuzzy, kde-format
#| msgid "Write a review!"
msgid "Write a Review…"
msgstr "Тақризеро нависед!"

#: discover/qml/ReviewsPage.qml:58
#, kde-format
msgid "Install this app to write a review"
msgstr "Барои навиштани тақриз ин барномаро насб намоед"

#: discover/qml/SearchField.qml:24
#, fuzzy, kde-format
#| msgid "Search"
msgid "Search…"
msgstr "Ҷустуҷӯ"

#: discover/qml/SearchField.qml:24
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search in '%1'…"
msgstr "Ҷустуҷӯ дар '%1'..."

#: discover/qml/SourcesPage.qml:13
#, kde-format
msgid "Settings"
msgstr "Танзимот"

#: discover/qml/SourcesPage.qml:93
#, kde-format
msgid "Default source"
msgstr ""

#: discover/qml/SourcesPage.qml:100
#, fuzzy, kde-format
#| msgid "Add Source..."
msgid "Add Source…"
msgstr "Илова кардани манбаъ..."

#: discover/qml/SourcesPage.qml:124
#, kde-format
msgid "Make default"
msgstr "Стандартӣ кунонидан"

#: discover/qml/SourcesPage.qml:210
#, kde-format
msgid "Increase priority"
msgstr ""

#: discover/qml/SourcesPage.qml:216
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr ""

#: discover/qml/SourcesPage.qml:221
#, kde-format
msgid "Decrease priority"
msgstr ""

#: discover/qml/SourcesPage.qml:227
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr ""

#: discover/qml/SourcesPage.qml:232
#, kde-format
msgid "Remove repository"
msgstr ""

#: discover/qml/SourcesPage.qml:243
#, kde-format
msgid "Show contents"
msgstr "Намоиш додани муҳтаво"

#: discover/qml/SourcesPage.qml:282
#, kde-format
msgid "Missing Backends"
msgstr ""

#: discover/qml/UpdatesPage.qml:12
#, kde-format
msgid "Updates"
msgstr "Навсозиҳо"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Update Issue"
msgstr "Мушкилии навсозӣ"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Technical details"
msgstr ""

#: discover/qml/UpdatesPage.qml:61
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr ""

#: discover/qml/UpdatesPage.qml:67
#, kde-format
msgid "See Technical Details"
msgstr ""

#: discover/qml/UpdatesPage.qml:83
#, kde-format
msgid ""
"If you would like to report the update issue to your distribution's "
"packagers, include this information:"
msgstr ""

#: discover/qml/UpdatesPage.qml:102
#, kde-format
msgid "Error message copied to clipboard"
msgstr ""

#: discover/qml/UpdatesPage.qml:134
#, kde-format
msgid "Update Selected"
msgstr "Навсозӣ инихоб шуд"

#: discover/qml/UpdatesPage.qml:134
#, kde-format
msgid "Update All"
msgstr "Ҳамаро навсозӣ кардан"

#: discover/qml/UpdatesPage.qml:174
#, kde-format
msgid "Ignore"
msgstr ""

#: discover/qml/UpdatesPage.qml:220
#, kde-format
msgid "Select All"
msgstr ""

#: discover/qml/UpdatesPage.qml:228
#, kde-format
msgid "Select None"
msgstr ""

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Restart automatically after update has completed"
msgstr ""

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Total size: %1"
msgstr ""

#: discover/qml/UpdatesPage.qml:278
#, fuzzy, kde-format
#| msgid "Restart"
msgid "Restart Now"
msgstr "Аз нав оғоз кардан"

#: discover/qml/UpdatesPage.qml:377
#, kde-format
msgid "%1"
msgstr "%1"

#: discover/qml/UpdatesPage.qml:393
#, kde-format
msgid "Installing"
msgstr "Дар ҳоли насбкунӣ"

#: discover/qml/UpdatesPage.qml:428
#, fuzzy, kde-format
#| msgid "Update All"
msgid "Update from:"
msgstr "Ҳамаро навсозӣ кардан"

#: discover/qml/UpdatesPage.qml:440
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr ""

#: discover/qml/UpdatesPage.qml:447
#, fuzzy, kde-format
#| msgid "More Information..."
msgid "More Information…"
msgstr "Маълумоти бештар..."

#: discover/qml/UpdatesPage.qml:475
#, fuzzy, kde-format
#| msgctxt "@info"
#| msgid "Fetching updates..."
msgctxt "@info"
msgid "Fetching updates…"
msgstr "Дар ҳоли бозёбии навсозиҳо..."

#: discover/qml/UpdatesPage.qml:488
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "Навсозиҳо"

#: discover/qml/UpdatesPage.qml:498
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr ""

#: discover/qml/UpdatesPage.qml:510 discover/qml/UpdatesPage.qml:517
#: discover/qml/UpdatesPage.qml:524
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "Нав аст"

#: discover/qml/UpdatesPage.qml:531
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "Омодагии навсозиҳо бояд тафтиш карда шавад"

#: discover/qml/UpdatesPage.qml:538
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr ""

#~ msgid "Featured"
#~ msgstr "Тавсияшуда"

#, fuzzy
#~| msgid "More..."
#~ msgid "Show more..."
#~ msgstr "Бештар..."

#, fuzzy
#~| msgid "Could not close Discover, there are tasks that need to be done."
#~ msgid "Could not close Discover because some tasks are still in progress."
#~ msgstr ""
#~ "Пӯшидани барномаи Кашфиёт ғайриимкон аст, зеро ки якчанд вазифа бояд ба "
#~ "анҷом расанд."

#~ msgid "Quit Anyway"
#~ msgstr "Ба ҳар ҳол пӯшидан"

#, fuzzy
#~| msgid "Loading..."
#~ msgid "Loading…"
#~ msgstr "Дар ҳоли боркунӣ..."

#~ msgid "Category:"
#~ msgstr "Навъ:"

#~ msgid "Author:"
#~ msgstr "Муаллиф:"

#~ msgid "Size:"
#~ msgstr "Андоза:"

#~ msgid "Source:"
#~ msgstr "Манбаъ:"

#~ msgid "Read the user guide"
#~ msgstr "Дастури истифодабарандаро хонед"

#, fuzzy
#~| msgid "Report a Problem:"
#~ msgid "Report a problem"
#~ msgstr "Гузориш дар бораи мушкилӣ:"

#~ msgid "%1 (Default)"
#~ msgstr "%1 (Стандартӣ)"

#, fuzzy
#~| msgid "Extensions..."
#~ msgid "Extensions…"
#~ msgstr "Васеъшавиҳо..."

#~ msgid "All updates selected (%1)"
#~ msgstr "Ҳамаи навсозиҳо интихоб карда шуданд (%1)"

#~ msgid "%1/%2 update selected (%3)"
#~ msgid_plural "%1/%2 updates selected (%3)"
#~ msgstr[0] "%1/%2 навсозӣ интихоб шуд (%3)"
#~ msgstr[1] "%1/%2 навсозӣ интихоб шуданд (%3)"

#~ msgid "OK"
#~ msgstr "Хуб"

#, fuzzy
#~| msgid "Install"
#~ msgctxt "Install the version of an app that comes from Snap, Flatpak, etc"
#~ msgid "Install from %1"
#~ msgstr "Насб кардан"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "The system requires a restart to apply updates"
#~ msgctxt "@info"
#~ msgid "The system must be restarted to fully apply the installed updates"
#~ msgstr ""
#~ "Барои татбиқ кардани тағйирот низоми шумо бояд аз нав оғоз карда шавад"

#~ msgid "%1, released on %2"
#~ msgstr "%1, дар %2 нашр шудааст"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "State being fetched"
#~ msgid "Loading..."
#~ msgstr "Дар ҳоли боркунӣ..."

#, fuzzy
#~| msgid "Write a review!"
#~ msgid "Write a Review..."
#~ msgstr "Тақризеро нависед!"

#~ msgid "Search..."
#~ msgstr "Ҷустуҷӯ..."

#~ msgid "Sorry..."
#~ msgstr "Таассуф..."

#~ msgid "Useful?"
#~ msgstr "Фоидаовар аст?"

#~ msgid "Update to version %1"
#~ msgstr "Навсозӣ кардан ба нашри %1"

#, fuzzy
#~| msgid "%1 → %2"
#~ msgctxt "Do not translate or alter \\x9C"
#~ msgid "%1 → %2%1 → %2%2"
#~ msgstr "%1 → %2"

#~ msgid "Write a review!"
#~ msgstr "Тақризеро нависед!"

#~ msgid "Be the first to write a review!"
#~ msgstr "Барои навиштани тақриз якум бошед!"

#~ msgid "Install and be the first to write a review!"
#~ msgstr "Насб кунед ва барои навиштани тақриз якум бошед!"

#~ msgid "Discard"
#~ msgstr "Рад кардан"

#~ msgid "User Guide:"
#~ msgstr "Дастури корбар"

#~ msgctxt "@info"
#~ msgid "Fetching Updates..."
#~ msgstr "Дар ҳоли бозёбии навсозиҳо..."

#~ msgctxt "@info"
#~ msgid "Up to Date"
#~ msgstr "Нав аст"

#~ msgctxt "@info"
#~ msgid "Submission name:<nl/>%1"
#~ msgstr "Номи пешниҳод:<nl/>%1"

#~ msgid "Review:"
#~ msgstr "Тақриз:"
